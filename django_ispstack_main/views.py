from django_template_limitless.django_template_limitless.views import *


def get_nav():
    nav = list()

    menu1 = MenuGroup()
    menu1.menu_name = "Access"

    menu2 = MenuGroup()
    menu2.menu_name = "Voice over IP"

    link1 = MenuLink()
    link1.link_name = "Trunks"
    link1.link_url = "/telephony/trunks"
    menu2.menu_items.append(link1)

    link2 = MenuLink()
    link2.link_name = "Numbers"
    link2.link_url = "/telephony/numbers"
    menu2.menu_items.append(link2)

    nav.append(menu1)
    nav.append(menu2)

    return nav
